# Data types

## Primitive Data Types

### boolean

* true or false
* Default value: false

### byte

* 8-bit signed integer
* Min value -128
* Max value 127
* Default value: 0

### short

* 16-bit signed integer
* Min value -32,768
* Max value 32,767
* Default value: 0

### int

* 32-bit signed integer
* Min value -2^31
* Max value 2^31-1
* default value: 0

### long

* 64-bit signed integer
* Min value -2^63 
* Max value 2^63-1
* Default value: 0L

### float

* 32-bit single-precision IEEE 754 floating point
* Default value: 0.0f

### double

* 64-bit double-precision IEEE 754 floating point
* Default value : 0.0d

### char

* 16-bit unicode character
* Min value '\u0000'
* Max value '\uffff'
* Default value: '\u0000'

## Object Data Types

### String

* Default value: null.

### Array

* Index starts with 0.
* Declaration: 
```java
    int [] example;
```
* Initialization:
```java
    example = new int[2];
```
