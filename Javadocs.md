
## Javadocs

* @author author
```java
/**
 * @author Name Surname <mail@example.org>
 */
```

* {@docRoot}

* @version version
```java
/**
 * @version     1.0
 */
```
* @since since
```java
/**
 * @since     1.0
 */
```

* @see see
```java
/**
 * @see     1.0
 */
```
* @param name description
```java
/**
 * @param     1.0
 */
```

* @return description
```java
/**
 * @return     1.0
 */
```

* @exception classname description
```java
/**
 * @exception     1.0
 */
```

* @throws classname description
```java
/**
 * @throws     1.0
 */
```
* @deprecated description
```java
/**
 * @deprecated     1.0
 */
```


* {@inheritDoc}

* {@link reference}

* {@linkplain reference}

* {@value #STATIC_FIELD}

* {@code literal}

* {@literal literal}

* {@serial literal}

* {@serialData literal}

* {@serialField literal}


