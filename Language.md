# Language

* Java is case sensitive.


## Identifiers

* Identifiers cant be a reserved word.
* Identifiers must start with a letter or the symbols $ and _.
* Identifiers must continue with letter, numbers or the symbols $ and _.

## Literals

### Integers

* int types are numbers.
* long types must end in L or l.
* Numberic systems:
  * Decimal: 123
  * Octal: 0x173
  * Binary: 0b1111011
* Underscore can be used to separate numbers:
  * Must be used between digits.
  * Can't be used at the end of a number
  * Can't be used at the begining of a number
  * Can't be used adjacent a decimal point.
  * Can't be used to separate a octal or a decimal radix. 

### Floating Point

* double can end in D or d.
* float must end in F or f.
* Scientific notation can be used: 2e2.

### Char

* Single unicode character denoted by ''.
* Unicade escape characters can be used: '\u0041'.

### String

* String of characters denoted by "".
* Special escaped characters:
  * \b - backspace
  * \t - tab 
  * \n - line feed
  * \f - form feed 
  * \r - carriage return
  * \" - double quote 
  * \' - single quote 
  * \\ - backslash


### null
* null can be used with reference types to denote that it does not point to an object. 


## Reserved words
abstract
assert
boolean
break
byte
case
catch
char
class
const
continue
default
do
double
else
enum
extends
final
finally
float
for
goto
if
implements
importinstanceof
int
interface
long
native
new
package
private
protected
public
return
short
static
strictfp
super
switch
synchronized
this
throw
throws
transient
try
void
volatile
while

*Reserved for literals*

true
false
null

*Reserved Special Identifiers*

var
_


## Comments

Three types of comments:

* Single line: 
```java
// This is a Single line Comment.
```
* Multiple lines:
```java
/* 
 * This is a Multiple line Comment.
 */
```
* Javadocs:
```java
/** 
 * This is a Javadoc.
 */
```
