# Operators

* Three kind of operators:
    * Unary
    * Binary
    * Ternary
* Division operator returns the floor value.
* Promotion of numbers:
    * When two values are of different type Java promotes to biggest type of the two.
    * When one value is floating-point and the other is integer Java will promote the integer to the floating point type.
    * When the value is byte, short or char Java will promote them to int everytime an binary operator is used.
    * The result will have the same value than the promoted values.


## Order of precendece

| Operator             | Precendence                               |       |
| -------------------- |-------------------------------------------| ------|
| Postfix              | expr++ expr--                             |       |
|  Unary               | ++expr --expr +expr -expr ~ !             |       |
| Multiplicative       | * / %                                     |       |
| Additive             | + -                                       |       |
| Shift                | << >> >>>                                 |       |
| Relational           | < > <= >= instanceof                      |       |
| Equality             | == !=                                     |       |
| Bitwise AND          | &                                         |       |
| Bitwise exclusive OR | ^                                         |       |
| Bitwise inclusive OR | \|                                        |       |
| Logical AND          | &&                                        |       |
| Logical OR           | \|\|                                      |       |
| Ternary              | booleanExpr?expr1:expr2                   |       |
| Assigment            | = += -= *= /= %= &= ^= \|= <<= >>= >>>=   |       |
